#Drexel University Heart Rate Processing
##Purpose
The purpose of this code is to modify existing heart rate processing with file automation, allowing people to process batchs of files instead of individuals.
##Use
Call the Matlab function with input and output directories as the arguments. If they are not provides, a UI will pop up to select the files. The Matlab code will process all .hrm files in the directory and output the processed files where specified. 

A sample command would be:  
hrProcessBatch('inputDir','./rawdata','outputDir','./outputs')  
Where './rawdata' and './outputDir' should be replaced with your intended directories. You can also leave them blank and you will be prompted by MATLAB for a directory. 