function [] = hrProcessBatch(varargin)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

p = inputParser;
p.addParameter('inputDir',[],@ischar);
p.addParameter('outputDir',[],@ischar);
p.parse(varargin{:});

inputDir = p.Results.inputDir;
outputDir = p.Results.outputDir;


currFullPath = mfilename('fullpath');
[currDir,~,~] = fileparts(currFullPath); %Gets current directory for the starting point in UI select directory
%Validate that the inputs are real directories, or if blank, ask for new
%ones
if(isempty(inputDir))
    inputDir = uigetdir(currDir,'Select Input Directory');
elseif(~isdir(inputDir))
    error('Input Directory does not exist, please pass a valid directory');
end

%Validating the output directory
if(isempty(outputDir))
    outputDir = uigetdir(currDir,'Select Output Directory');
elseif(~isdir(outputDir))
    mkdir(outputDir); %makes the output directory if it doesn't exist
    fprintf('Creating Directory %s\n',outputDir);
end

%Finding all of the .hrm files in the input directory
fileListStruct = dir([inputDir,'\*.hrm']);

%Iterates over the .hrm files in directory and runs 
for i = 1:length(fileListStruct)
    matlabHRMcode(fileListStruct(i).name,outputDir);
end

end

