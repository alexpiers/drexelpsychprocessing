%By Zoe Zhang, Jan 22, 2015
%read in data set
%Modified by Alex Piers, Aug-08-2016
function [] = matlabHRMcode(fileName,outputDir)

inputfile = fopen(fileName);

% Ignore all lines of text, until we see one that just consists of this:
startString = 'HRData';

while 1
    tline = fgetl(inputfile);
    tline=regexprep(tline,'[^\w'']','');
    if   strcmp(tline, startString)
    % Break if we hit the start marker
        break
        
    end
end

%Process all remaining lines
your_data = []; 
tline = fgetl(inputfile);
%whether we reach the end of the file
    while(~feof(inputfile))
    tline = fgetl(inputfile);
    %Getting rid of non-numbers
    tline = regexprep(tline,'[^0-9\s+-.eE]','');
    %string to number
    your_data = [your_data; str2num(tline)];
end
fclose(inputfile);

%code all values less than 60 or greater than 200 as missing NaN
ind=find(your_data<60|your_data>200);
your_data(ind)=NaN;

%delete any data after 1 hour 30 minutes (after 1080 readings)
if length(your_data)>1080
    your_data=your_data(1:1080);
end

%ignore missing data first
data=your_data(~isnan(your_data));
k=1;
val=[];
rep=[];
temp=[];
%the threshold for the difference between two numbers
%can be changed if needed
threshold=5;
for i = 1:(length(data)-1)
    j=i;
        %compare whether the difference is smaller than the threshold
    while (abs(data(i)-data(j+1))<= threshold)
        %if smaller than the threshold, then add 1 to k
        k=k+1;
        %increase the index
        j=j+1;
        %if we reach the end, then break
        if j==length(data)
            break
        end
    end
    %take the min of these consecutive numbers
    temp=min(data(i: i+k-1));     
          val=[val;temp];
          rep=[rep;k];
          %reset k to be 1
          k=1;
    
end
res=[val' ; rep']';
%the threshold for the number of times a value appears
%can be changed if needed
threshold2=6;
%it has to be maintained at least 6 times
freqvalue=res(find(res(:,2)>= threshold2 ),:);
%find the maximum heart rate
htr=max(freqvalue(:,1));
%write the processed data to a text file
dlmwrite(strcat(outputDir,'/',sprintf('%s-processed_data.txt',fileName)),your_data,'delimiter','\t','precision',6);
%write the data value and the number of repetitions to a text file
dlmwrite(strcat(outputDir,'/',sprintf('%s-max_HeartRate.txt',fileName)),htr,'delimiter','\t','precision',6);
end


